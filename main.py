import numpy as np

def main():
  # Leitura dos inputs
  n, m = list(map(int, input().split()))
  c = np.array(list(map(float, input().split())))
  A = np.array([list(map(float, input().split())) for _ in range(n)])

  # Funções úteis
  def is_covered_by(a, S):
    return np.any(A[a, list(S)])

  def find_min_gap_id(gaps, a):
    min_gap = np.min(gaps[np.where(A[a, :])])
    return next(i for i, x in enumerate(gaps) if x == min_gap)

  # Inicialização de variáveis
  x = np.zeros(n)
  C = set()

  # Enquanto ainda existe 'a' que não esteja coberto por nenhum subconjunto de C
  while np.any([not is_covered_by(a, C) for a in range(n)]):
    # Encontra o primeiro 'a' não coberto 
    a = next((a for a in range(n) if not is_covered_by(a, C)), None)

    # Encontra o quanto falta para cada posição de A^t x <= c saturar
    gaps = c - np.transpose(A) @ x

    # Encontra no vetor criado anteriormente o menor valor (e que corresponda a um subconjunto que engloba 'a')
    best_gap_i = find_min_gap_id(gaps, a)

    # Atualiza 'x' para que ele faça a desigualdade ficar justa na posição encontrada anteriormente
    x[a] += gaps[best_gap_i]

    # Encontra o conjunto S que corresponde a igualdade A^t x = c (e que possui a)
    all_S = np.where(np.transpose(A) @ x == c)[0]
    S = next(x for x in all_S if A[a, x])

    # Adiciona em C
    C.add(S)
  
  # Output
  print(" ".join([str(int(i in C)) for i in range(m)]))
  print(" ".join(str(int(i)) for i in list(x)))

if __name__ == '__main__':
  main()